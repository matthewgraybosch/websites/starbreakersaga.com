# About the Author

According to official records maintained by the state of New York,
[Matthew Graybosch](https://matthewgraybosch.com) was born somewhere
on Long Island in 1978.

Urban legends suggest he might be Rosemary’s Baby or the result of
top-secret DOD attempts to continue Nazi experiments combining human
technology and black magic. The most outlandish tale suggests that he
sprang fully grown from his father’s forehead with a sledgehammer in
one hand and the second edition of *The C Programming Language* in the
other &mdash; and has been giving the poor man headaches ever since.

The truth is more prosaic. Matthew is an author from New York who
lives with his wife and cats in central Pennsylvania. He is also an
avid reader, a long-haired metalhead, and an unrepentant nerd with
delusions of erudition.

*Without Bloodshed* (2013) is his first published novel, followed by
*Silent Clarion* (2016). He has also written short stories like "The
Milgram Battery" and "Limited Liability". He is allegedly working on
new Starbreaker stories like *When You Don't See Me* and *Dissident
Aggressor*, but sources within the NSA suggest he’s doing nothing of
the sort.

His day job is software development, and we're not sure how he remains
sane. We could ask, but we suspect he’d say, "I'm not sane, but hum a
few bars and I'll fake it."

![a photograph of the author](/images/author-nyc.jpg)
Matthew Graybosch atop Rockefeller Center in New York. Photo by
Catherine Gatt.
