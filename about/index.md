# About Starbreaker

Don't know what you're getting yourself into? Keep reading.

If you're already a fan, this might give you some interesting background.

## What Is Starbreaker?

The *Starbreaker* saga is a great big sloppy pastiche of
awesomeness, set in a future similar ours where... 

* First contact happened over 10,000 years ago.
* Aliens have lived among us throughout the history of civilization.
* Humanity has begun to colonize the entire solar system while still
  healing the scars of a global collapse they remember as *Nationfall*.
* Benevolent artificial intelligence works alongside humans.
* An elite corps of tribunes investigates and punishes abuses of power
  wherever they occur under the aegis of the Phoenix Society.
* All-too-human androids rock out with swashbuckling sopranos.
* The gods came from outer space.
* An ancient evil is bound beneath the ice of Antarctica.
* The dark lord wears white, already rules the world, and is
trying to save it.

If you can't tell whether it's science fiction or fantasy, don't
worry. It's both and neither. It's [science
fantasy](http://www.fantasy-magazine.com/non-fiction/is-it-science-fiction-or-science-fantasy/). I
mean, what else should I call it?

Starbreaker is a saga where all-too-human androids and swashbuckling
soprano catgirls expose corruption and fight demons from outer space
on a near-future alternate Earth. Humanity has risen from global
collapse to an interplanetary Renaissance in less than a
century. Meanwhile, a clandestine war between demons threatens to go
hot once more. Morgan, Naomi, and their friends are caught in the
middle and they are *profoundly* displeased.

## How It All Began

I began writing the Starbreaker saga in 1996, seeking to write a
literary rock opera paying tribute to classic heavy metal and
progressive rock. The sensible thing to do would have been get a few
other people together and start a tribute band, but I already knew I
wasn't enough of a musician to do the job.

The epic first took the form of a previously unpublished novel
in 2009. After years of work, I re-imagined the Starbreaker saga as a
quartet of novels, and released the first of them in 2013. After
getting side-tracked with a prequel featuring a major supporting
character as its protagonist -- and the responsibilities of a married
man with a day job -- I'm ready to continue the saga.

## Who Is Starbreaker For?

It's for me, first and foremost, but it wouldn't be as much fun if I
tried to bogart the awesomeness. Pick a fandom. Any fandom. Chances
are you'll feel right at home.

Unless you're some kind of fascist. In the Starbreaker setting, the
only good fascist is a *dead* fascist.

I'm not going to deny that Starbreaker is political, but it's no more
so than Queensrÿche's 1988 album, *Operation: Mindcrime*.

## Where Can I Get Starbreaker?

If you're interested, I'm releasing the rough cuts online. I'm working
on making works of varying lengths available so that you can fit
Starbreaker into your life.

### Stories

No more than 5,000 words. Readable in one or two sessions.

* "The Milgram Battery"
* "The Holiday Rush"
* "Tattoo Vampire"
* "Limited Liability"
* ["Thirteen Cuts"](/stories/thirteen-cuts.html)

### Novelettes

Between 5,001 to 10,000 words. May require up to four sessions.

Want more, but don't have time for a novella or a full novel?

* *Steadfast* &mdash; the original version of [*Silent Clarion*](/novels/silent-clarion.html)

### Novellas

Between 10,001 to 25,000 words. May require up to twelve sessions.

* *When You Don't See Me* (WIP)

### Novels

At least 50,000 words. These could take a while.

**the Starbreaker saga**

* [*Without Bloodshed* &mdash; Part One](/novels/without-bloodshed.html)
* *Dissident Aggressor* &mdash; Part Two (WIP)
* *Shattered Guardian* &mdash; Part Three (on hold)
* *Unyielding Defiance* &mdash; Part Four (in planning)

**Starbreaker novels**

* [*Silent Clarion*](/novels/silent-clarion.html)

![a collage of characters from Starbreaker](/images/starbreakercollage.jpg)
Some of the cast of Starbreaker. Artwork by [Harvey Bunda](https://harveybunda.com) commissioned by [the author](https://matthewgraybosch.com).
