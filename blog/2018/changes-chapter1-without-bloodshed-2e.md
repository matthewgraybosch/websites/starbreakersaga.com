# Changes for Chapter 1 of Without Bloodshed 2E

Many of the interactions in "The Unforgiven", Chapter 1 of *Without
Bloodshed*, will need to be re-written. This will change the rest of
the novel considerably. However, it's necessary because I want to
account for the events of *Silent Clarion*, set things up for
*Dissident Aggressor*, and improve the characterization and plotting
in *Without Bloodshed*.

## Naomi Bradleigh

It's one thing to have Naomi come to the door sleepy, but she should
come armed with a naked sword because it's long past midnight.

Naomi should recognize Imaginos on sight. After all, how many
blue-eyed, white-haired bishounen are there in the world? Of that
small set, how many of them wear double-breasted white suits with blue
cravats and sapphire cufflinks?

Naomi should *not* be friendly to Imaginos. They have history from
*Silent Clarion*.

Naomi should address Imaginos by his "true name", and then by the
alias he used in *Silent Clarion*: "Ian Malkin".

Naomi should recognize that Imaginos has an unconscious woman who
looks a hell of a lot like Christabel Crowley in his arms and suspect
nefarious intentions on his part.

Never mind the kind Winter Solstice wishes. Sure, they establish early
on that the Starbreaker setting isn't on a Christian calendar, but
it's out of character given what Naomi knows about Imaginos.

We don't want to turn Naomi into a complete bitch lest she put readers
off, but she should be polite, firm, and highly suspicious.

## Imaginos/Ian Malkin/Isaac Magnin

I need to do a better job of showing that Imaginos and Isaac Magnin
are the same person.

Naomi recognzing Imaginos for who he is and then calling him "Ian
Malkin" is an opportunity for Imaginos to imply that he's using a new
identity nowawadays, and explain that he's now "Isaac Magnin".

Imaginos should also recognize that Naomi has the true Starbreaker.

Naomi possessing the true Starbreaker requires changes in Desdinova's
scenes later on.

Imaginos should insist that despite what Naomi thinks of him, he's
here to help Christabel, because he promised to see her safely home.

When Naomi insists that Imaginos leave Christabel on the couch, and
settles into one of the armchairs by the fire to keep an eye on her,
he should temporarily retreat.

## Ashtoreth/Elisabeth Bathory

We should make Ashtoreth a viewpoint character, and have her show up
as Imaginos leaves Christabel's house. This will split "The
Unforgiven" into two scenes, but I think it's worth it.

She already has a good idea as to what's going on because she saw
Naomi greet Imaginos with the Starbreaker in her hand.

She offers to help Imaginos, since Naomi isn't likely to go to sleep
on her own, but Imaginos insists on dealing with it himself.

Imaginos leads Ashtoreth through Christabel's house, first disabling
her household AI daemon Aleister.

Ashtoreth should still remonstrate with Imaginos, insisting that he
might be able to get what he needs by *talking* to Morgan, but to no
avail.

Ashtoreth doesn't fully understand Imaginos' purpose, but all he'll
say is that Christabel must die if Annelise is to live again. The
Christabel/Annelise connection will come up early in the next novel,
*Dissident Aggressor*, in the first part: "When You Don't See Me".

It isn't until she gets into the limousine that brought Imaginos to
Crouch End and sees the other passenger that she begins to
understand. But before she can demand an explanation, Imaginos tells
her he's counting on her to help make it a show to remember. The limo
kicks into gear, leaving Ashtoreth with the original Christabel
Crowley.

The question is, do we give the line about how this should be an
interesting show to Ashtoreth, or find away for Imaginos to keep it?
