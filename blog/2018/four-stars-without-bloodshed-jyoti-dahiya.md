# Four Stars for *Without Bloodshed* from Jyoti Dahiya

I got an unexpected birthday present from a fan this afternoon: a
four-star review for [*Without
Bloodshed*](/novels/without-bloodshed.html). Here's what she said on
[Google+](https://plus.google.com/u/0/+JyotiDahiya/posts/1ZyGveWU9BX):

> In the meantime, cryptic conversations and a slow leak of information
> build the suspense, leavened liberally with action sequences,
> betrayal, honour, technology indistinguishable from magic, and other
> page-turning stuff. Some of the stuff that will keep me up at night
> includes why Morgan and Naomi were running a band called Crowley's
> Thoth, what the ends of Imaginos are, why congenital pseudofeline
> morphological disorder appeared among humans, how many of the people
> around are humans at all, and why on earth Imaginos is so cavalier
> about his daughter's well-being himself while visiting death on those
> who harm a single hair on her head. Who is this Imaginos guy anyway,
> and who is this Sabaoth he strives against, and how come their enmity
> is older than the pyramids? And I already want Witness Protocol and
> Tesla points in real life, though suborbital flights I can probably do
> without.
> 
> Yeah, you get the bug too, don't you? Read and find out for
> yourself.

Ms. Dahiya has a longer review on
[Goodreads](https://www.goodreads.com/review/show/2537293061?book_show_action=false),
too. Sweet!
