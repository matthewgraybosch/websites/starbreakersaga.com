# Blog Posts for 2018

This is everything I've posted in 2018.

* [4 Stars for *Without Bloodshed* from Reader Jyoti Dahiya](/blog/2018/four-stars-without-bloodshed-jyoti-dahiya.html "24 Sep 2018")
* ["Thirteen Cuts"](/blog/2018/new-story-thirteen-cuts.html "18 Sep 2018")
* [Welcome Back My Friends](/blog/2018/welcome-back-my-friends.html "10 Sep 2018")
