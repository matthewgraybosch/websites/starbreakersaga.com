# New Story: "Thirteen Cuts"

I haven't done much writing since my wife was diagnosed with
early-stage breast cancer, but she had her final round of chemotherapy
last week. Knowing she's going to be all right was the bit of relief I
needed to do a bit of writing.

["Thirteen Cuts"](/stories/thirteen-cuts.html) didn't start out as a
Starbreaker story, but arose out of an internet argument on how to
retain the death penalty while strongly discouraging its use. Rather
than get involved in the argument myself, I did the sensible thing and
went to bed because it was late.

Only instead of going to sleep as sensible adults typically do once in
bed, I kept turning over the idea that a judge and jury that sentenced
somebody to death should be required to carry out the sentence
themselves, immediately, knowing they themselves might stand trial for
murder of evidence comes to light that would have exonerated the
accused.

Rather than sleep, I got up at two in the morning, had the first half
of the story bashed out by the time my wife woke up, and had to
explain to her why I was sitting in my study banging away at six in the
morning. I finished the story over the weekend, and after a couple of
independent editors told me there wasn't anything they could do with
the story, I had a choice:

1. Try to sell it.
2. Post it online.

I really don't have the spoons to deal with all of the bullshit that
comes with being a professional SF author, so here it is for you to
enjoy.

[Read "Thirteen Cuts" online...](/stories/thirteen-cuts.html)
