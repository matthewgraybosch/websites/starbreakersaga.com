# Welcome Back My Friends

It's been a while since I maintained a proper website for the
Starbreaker saga, but things have been a bit crazy over the last
couple of years.

However, with the housing situation secured and my wife on track to
complete treatment for breast cancer, it's time to turn my attention
away from the real world and back to a better world...

...a world where corrupt scumbags like the *entire Republican Party*
are in jail for corruption and tyranny...

Sorry about that. I'm typing this at my day job in a break room with a
muted TV tuned to CNN, which is reporting on the President's latest
[big lie](https://en.wikipedia.org/wiki/Big_lie).

Actually, I'm *not* sorry. The whole Starbreaker saga is inspired in
part by my frustration with US politics in general and my frustration
with the creeping fascism of Republicans in particular. Abuses of
power in government, in business, and in the church should not be
tolerated. They should be zealously investigated, and those
responsible exposed, tried, and punished if proven guilty.

The Adversaries in Starbreaker -- those libertarian musketeers who
bring politicians, clergy, and corporate fatcats alike to heel -- are
themselves inspired by a historical institution: the [Tribune of the
Plebs](https://en.wikipedia.org/wiki/Tribune_of_the_Plebs).

In the early years of the Roman Republic, the plebians (the working
classes) protested years of abuses by the ruling patricians by leaving
the city *en masse*. The Senate dispatched a representative to
negotiate, and the resulting negotiations established a small group of
representatives charged with defending the interests of the lower
classes by vetoing actions by magistrates and senators they deemed
harmful to the plebian class. In addition...

* Only plebians could become tribunes of the plebs
* Tribunes were sacrosanct; the plebians had the right to kill anybody
  who harmed or interfered with a Tribune

Of course, the Tribune of the Plebs was a tragically flawed
institution. It was only truly effective in the city of Rome and its
immediate vicinity because tribunes had to veto magistrates and
senators *in person*.

However, the Adversaries are not the new Tribune of the
Plebs. Instead, they're sworn to back the tribunes' authority. In the
world of Starbreaker, no decision may be made in business, in
government, or in the church without the presence of a tribune who may
veto the decision if they even *suspect* that the decision is harmful
to the public good.

These tribunes are lightning rods, drawing the anger of those in
power. It is the Adversaries who defend them, and if necessary avenge
them.

This being the case, I have two aims for my Starbreaker saga:

1. I want to show a world where the rights of *all* individuals are
   consistently and ferociously protected.
2. I want to show how a world where individuals are empowered by the
   presence of people willing to uphold their rights by diplomacy and
   force of arms might band together to resist a tyrant operating on
   an interplanetary scale.
   
Moreover, I want these stories available for *everybody* to read and
enjoy. To that end, I'll be posting a new and annotated edition of
*Without Bloodshed* as a serial, and continuing the story from
there. Other Starbreaker stories will appear here, as well.

I hope you'll stick around as I get this site up and running and add
more good stuff to read.
