# Jude Cole

A young gay man who works for a Manhattan advertising firm. He was one
of [Dr. Gilbert Porter's](/characters/porter-gilbert.html) patients,
seeing him for help getting over his guilt over a lover's suicide, and
later to deal with his guilt in the judicial murder of Jean Vorbes.

His testimony was instrumental in the [Phoenix
Society](/organizations/phoenix-society.html)'s prosecution of
Manhattan district attorney Jason Lane on a multitude of abuses of
power, though his own involvement was punished with exile from Gaia to
an artifical habitat orbiting Aphrodite.

## Appearances

* ["Thirteen Cuts"](/stories/thirteen-cuts.html) &mdash; protagonist,
  viewpoint character
