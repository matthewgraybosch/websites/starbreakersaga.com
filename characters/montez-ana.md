# Ana Montez

A medical receptionist who works for [Dr. Gilbert
Porter](/characters/porter-gilbert.html). Unlike her predecessors, she
usually refrains from any sort of talk not directly related to work
while on the job. While this often makes her seem cold or standoffish,
it suits Dr. Porter.

She ended up working for Dr. Porter until his retirement, but because
they never talked about anything but work he had no idea she was a
lesbian until after his retirement when he proposed marriage.

## Appearances

* ["Thirteen Cuts"](/stories/thirteen-cuts.html)
