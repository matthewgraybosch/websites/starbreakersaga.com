# Dr. Gilbert Porter

Dr. Gilbert Porter is a psychiatrist based in Brooklyn. Having seen
what the Patch did to the rest of his family, he prefers to avoid the
use of drugs in his own practice, leaning instead on
cognitive-behavioral therapy. He retained [Ana
Montez](/characters/montez-ana.html) as his receptionist until his
retirement, and was subsequently rebuffed when he proposed marriage.

He was instrumental in the prosecution of Jason Lane, since his
testimony bolstered the credibility of one of the Phoenix Society's
central witnesses, [Jude Cole](/characters/cole-jude.html).

## Appearances

* ["Thirteen Cuts"](/stories/thirteen-cuts.html) &mdash; protagonist,
  viewpoint character
