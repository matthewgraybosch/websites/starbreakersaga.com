# About This Website

**starbreakersaga.com** is owned and operated by [Matthew
Graybosch](https://matthewgraybosch.com). Email all questions to [the
author](mailto:matthew@starbreakersaga.com).

**starbreakersaga.com** made with &hearts; in Harrisburg, PA and hosted by
[Dreamhost](http://dreamhost.com).

**starbreakersaga.com** is built with the following tools:

* [OpenBSD](https://openbsd.org)
* [GNU Emacs](https://www.gnu.org/s/emacs/)
* A customized version of Roman Zolatarev's ["ssg" shell script](/ssg)
* [lowdown(1)](https://kristaps.bsd.lv/lowdown/)
* [rsync(1)](https://rsync.samba.org/)
* [entr(1)](http://entrproject.org/)

All source files are stored on Gitlab at [https://gitlab.com/matthewgraybosch/websites/starbreakersaga.com](https://gitlab.com/matthewgraybosch/websites/starbreakersaga.com)

## Style Info

The website's stylesheet is designed to honor your reading
preferences. It does not explicitly specify any fonts, and specifies
the font size as "Medium". Headers are in serif fonts. All other text
is sans-serif.

## Subscription Info

You can subscribe to this website by adding our [RSS feed](/feed.xml) to your
favorite reader. If you run some kind of free Unix I strongly
recommend [newsboat](https://newsboat.org). Roman Zolatarev has an
excellent [setup guide](https://www.romanzolotarev.com/newsboat.html).

## License Info

All original content is available under the Creative Commons [BY-SA
4.0](https://creativecommons.org/licenses/by-sa/4.0/) free culture
license.

## Privacy Policy

If this were a corporate site we'd have a separate page full of
bullshit we paid some lawyer to write for us. However, we aren't a
corporation so we'll keep it simple.

**We will not sell your personal details or give them away unless
legally compelled to do so by a valid court order.**

### The Devil's in the Details

* Dreamhost logs all visits on server-side HTTP logs. These logs
  identify visitors by IP address.
* This website contains no client-side analytics. You can verify this
  for yourself by viewing pages' source code in a desktop browser like
  Firefox.
* We don't collect any data about you unless you sign up for Matthew
  Graybosch's mailing list, ["A Day Job and a
  Dream"](https://getrevue.co/profile/matthewgraybosch).  Any data you
  provide while subscribing is also governed by Revue's [privacy
  policy](https://getrevue.co/privacy/).
