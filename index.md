# Welcome to a Different World

Looking for something a bit *different* to read? We've got just the
thing: **Starbreaker**. 

All-too-human androids and swashbuckling sopranos unravel corruption
and fight demons from outer space. What's not to like?

## Posts

* [Changes for Chapter 1 of Without Bloodshed 2E](/blog/2018/changes-chapter1-without-bloodshed-2e.html "9 Oct 2018")
* [4 Stars for *Without Bloodshed* from Reader Jyoti Dahiya](/blog/2018/four-stars-without-bloodshed-jyoti-dahiya.html "24 Sep 2018")
* [New Story: "Thirteen Cuts"](/blog/2018/new-story-thirteen-cuts.html "18 Sep 2018")
* [Welcome Back My Friends](/blog/2018/welcome-back-my-friends.html "10 Sep 2018")

For older posts, please visit the [archive](/blog/).

## Basic Information

* [About Starbreaker](/about/) &mdash; more about Starbreaker
* [About the Author](/about/bio.html) &mdash; author bio

## Other Pages

* [Site Map](/sitemap.html) &mdash; every page on this site
* [Site Info](/colophon.html) &mdash; implementation details
