# Silent Clarion

**a Starbreaker novel, by [Matthew Graybosch](https://matthewgraybosch.com)**

My curiosity might get me killed. I thought I needed a vacation from
my duties as an Adversary in service to the Phoenix Society. After
learning about unexplained disappearances in a little town called
Clarion, I couldn't stop myself from checking it out.

Now I must protect a witness to two murders without any protection but
my sword. I must identify a murderer who strikes from the shadows. I
must expose secrets the Phoenix Society is hellbent on keeping buried.

I have no support but an ally I dare not trust. If I cannot break the
silence hiding what happened in Clarion's past, I have no future. I
must discover the truth about Project Harker. Failure is not an
option.

*Silent Clarion* is a new-adult science-fiction thriller by Matthew
Graybosch, set before the events of the Starbreaker novels. Meet Naomi
Bradleigh as an Adversary, seventeen years before [*Without Bloodshed*](/novels/without-bloodshed.html).
