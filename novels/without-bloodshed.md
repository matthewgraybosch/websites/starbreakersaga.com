# Without Bloodshed

**Part One of Starbreaker, by [Matthew Graybosch](https://matthewgraybosch.com)**

"All who threaten me die."

These words made Morgan Stormrider's reputation as one of the Phoenix
Society's deadliest IRD officers. He served with distinction as the
Society's avenger, hunting down anybody who dared kill an Adversary in
the line of duty. After a decade spent living by the sword, Morgan
seeks to bid a farewell to arms and make a new life with his friends
as a musician.

Regardless of his faltering faith, the Phoenix Society has a final
mission for Morgan Stormrider after a dictator's accusations make him
a liability to the organization. He must put everything aside, travel
to Boston, and prove he is not the Society's assassin. He must put
down Alexander Liebenthal's coup while taking him alive.

Despite the gravity of his task, Morgan cannot put aside his
ex-girlfriend's murder, or efforts to frame him and his closest
friends for the crime. He cannot ignore a request from a trusted
friend to investigate the theft of designs for a weapon before which
even gods stand defenseless. He cannot disregard the corruption
implied in the Phoenix Society's willingness to make him a scapegoat
should he fail to resolve the crisis in Boston without bloodshed.

The words with which Morgan Stormrider forged his reputation haunt him
still.
