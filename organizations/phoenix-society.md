# the Phoenix Society

Originally chartered as a non-government organization (NGO), the
Phoenix Society soon became the de facto government of the planet Gaia
after [Nationfall](/history/nationfall.html), the near-extinction of
humanity on the planet Gaia following their adoption of [the
Patch](/technology/the-patch.html).
