# Thirteen Cuts

**by [Matthew Graybosch](https://matthewgraybosch.com)**

*The following is a work of fiction. It contains discussions of
violence, racism, suicide, and toxic masculinity that may not be
appropriate for all readers.*

A printer-friendly version of this story is available. [Download "Thirteen Cuts".](/downloads/thirteen-cuts.pdf "Typeset for YOUR convenience!")

## I

"[Dr. Porter](/characters/porter-gilbert.html), your one-thirty is
waiting for you." As usual, [Ana Montez](/characters/montez-ana.html)
wasted no effort on pleasantries. Her predecessors would ask
Dr. Porter how his lunch was. Worse, they would want to talk about
their lunches, forcing him to spend time on them that he might have
put to better use with a patient. Ms. Montez' brusque efficiency had
proved a welcome change.

"He seemed agitated," said Ms. Montez in a softer voice despite the
empty waiting room, "So I asked him to wait in your office."

"Thank you," said Dr. Porter as he hung up his coat. "[Jude
Cole](/characters/cole-jude.html), if memory serves."

Ms. Montez rose, and held up a manila folder. "Here's his file."

"Perfect. Thanks again." Accepting the folder, Dr. Porter opened the
door to his office and went to his desk with the confident stride of a
man sure that everything was in its proper place.

It was not until he had reviewed his case notes that he turned his
attention to the small, pale man sitting hunched forward on the
couch. He kept rubbing his slim-fingered hands together as if trying
to wash them without soap or water, a tic Dr. Porter did not recall
from Mr. Cole's previous sessions.

Making a note, Dr. Porter cleared his throat. "Hello again,
Mr. Cole. I hope I didn't keep you waiting too long. I'm glad to see
that your absence has not left you much the worse for wear."

Mr. Cole did not look at him, but spoke softly and rapidly. It was
another change for Dr. Porter to note. "I was early," said he. "I'm
sorry it's been so long, but something came up suddenly, and I
couldn't even call to cancel."

Dr. Porter was grateful for the opening; he had worried for a moment
that he might have to approach the subject of Cole's absence obliquely
after first re-establishing their previous rapport. "Would you like to
tell me what happened? We were so close to working through the worst
of your grief at the loss of your partner."

"I know," said Mr. Cole. "I thought I was ready to let Patrick go. I
wanted to tell you at the next session, but I had jury duty."

Dr. Porter referred to his notes for the date of Cole's last
visit. "You were a juror for six months? It must have been a complex
trial. Would you like to tell me about your experiences?"

"It wasn't that complex. At least it didn't seem that way at first. It
was just a holdup gone violent; the defendant thought the clerk was
reaching under the counter for a weapon, and shot her in the head. The
police and the forensics experts told us they had the right guy, that
they had identified him from the security video and found gunpowder
residue on his hands and clothes. First degree murder, because the
defendant was committing armed robbery and killed a woman in the
process. The DA pushed for the death penalty."

Dr. Porter winced. Patients with post-traumatic stress — especially
those called upon to kill for the greater good — were both his most
lucrative and his most maddening cases. They were lucrative because
post-traumatic stress was not something he could fix with a
prescription, and maddening because in most cases he was nothing but
the catalyst for the patient working through their issues on their
own.

Jude Cole looked to be one of those cases. If he had served on a jury
that not only found somebody guilty, but found their crimes so heinous
that the death penalty was justified, then the laws governing Gaea
would have forced him to help carry out the sentence with his own
hands. His would have been one of the thirteen ritual cuts inflicted
on the condemned as the judge and jury offered blood sacrifice to
justice on the courthouse lawn.

To be called upon to kill in public was hard enough, but worse was the
burden of certainty required of the judge and jurors. There was no
room for doubt once capital punishment was on the table, for if the
judge and jury put an individual to death for crimes in which they
shared no guilt, they themselves could be charged with first degree
murder.

"It must have been a heavy responsibility, having another person's
life in your hands," said Dr. Porter. "Did you have doubts about the
prosecution's case?"

Cole remained silent for several minutes before finally saying,
"Yeah. I should have listened to them, too."

Dr. Porter nodded. "Tell me about the doubts you felt while serving,
Mr. Cole."

Another long silence from Cole preceded his answer. "Have you ever
gotten the hard sell from somebody? Like they were just pushing too
hard to get you to buy something or believe something?

"That was the DA from opening argument to closing statement. He was
sure he had the right guy, and he wanted us to be sure. They kept
showing us photos that were supposed to be enhanced stills from the
security tape, but when the defense pushed to have the actual tape
played in court, the video just wasn't sharp enough for the guy in the
video to look like the guy in the photos."

Dr. Porter nodded. "Did you suspect that the prosecutor or the police
had doctored the photos to look like the defendant?"

"Doctored?" Cole snorted. "I think they took stills from the video and
pasted the poor schmuck's mugshot into them. I still work in
advertising over at Harris, Smith, and Murray on Madison and
Forty-Fourth, and before that I was a creative for Burr & Di'Anno in
LA. I know how it looks when a talented amateur runs photos through
Krita or Gimp. They leave imperfections that a professional wouldn't."

"What about the video?"

"Whoever the prosecutor got to edit the video had no idea what they
were doing, and they must have been rushing like mad bastards because
the defense attorney threatened to file a motion to dismiss on due
process grounds if the DA didn't comply with the discovery request
forthwith."

"That's the sort of lawyer I'd want on my side," said Dr. Porter. "A
real shark."

"Yeah. Shame the jury was rigged against the defense from the start."

"We'll come back to that," said Dr. Porter. "It seems strange that you
never refer to the defendant by name."

He did not expect his patient's reaction. Cole began to cry, hiding
his face in his hands as he sat on the edge of the couch and
sobbed. "His name was Jean Vorbes, and we killed him."

Instinct and experience demanded that Dr. Porter prod the patient for
further detail, that this tearful admission lay at the root of Cole's
distress. "How did you kill Jean Vorbes?"

"We convicted him of first-degree murder, armed robbery, and
possession of a concealed mil-spec weapon. We sentenced Vorbes to
death, and the judge concurred. The bailiff tied Vorbes to a stake
outside the front of the courthouse, and gave us each a knife. The
bailiff made us look Vorbes in the eye as we stabbed him. I was the
last, before the judge, and..."

Dr. Porter waited for several minutes before giving Cole another
prod. His voice was soft and gentle as he prompted his patient. "And
what, Mr. Cole?"

"He looked me in the eye. He said he understood why. He forgave me."

Porter had to wait several minutes before Cole was calm enough to
answer his next question. "I suppose that if Jean Vorbes knew the
trial was rigged against him, he would have been relieved to see his
ordeal ended after six months."

"The trial didn't actually last that long. Just three weeks. The DA
didn't want us thinking too hard about the state's case."

"So, why didn't you come back sooner?"

"I honestly thought that I'd eventually get over it, that with time
the nightmares would stop."

Dr. Porter nodded. "And they haven't yet? Want to tell me about them?"

"It's the same damn thing every night. I'm there by myself, and I've
got to strike a blow for each of the other jurors, one for myself, and
then the killing stroke. Every time I stick the knife in, he asks me
why. Like he doesn't understand."

Dr. Porter wrote this down verbatim. Doing so gave him time to
consider the dream. Being a recurring nightmare, he reasoned, it had
to possess some significance to Cole that the patient himself did not
understand. "It sounds as though you are taking all of the guilt for
executing Jean Vorbes onto your own shoulders, but why?"

Cole's left eyebrow twitched as he frowned, drawing Dr. Porter's
attention to a scar that he felt reasonably confident had not been
there six months ago. Putting his pen aside, Porter approached Cole
for a closer look, and found that Cole's nose also seemed crooked, as
if it had been broken. "Have you been assaulted recently?"

"How did you know?"

"You have a scar on your left eyebrow, and your nose looks like it has
been broken. Did this happen after the Vorbes trial? Did one of his
relatives come after you?"

"God, no. I wish they had," said Cole. "It was the jury foreman,
Rizzo. The guy was a real piece of work, kept saying 'We gotta make
this asshole pay,' but since Vorbes was from Port au Prince it should
be pretty obvious that 'asshole' isn't the word he used."

Dr. Porter nodded. "I can guess. I've heard it a few times myself. I
appreciate your restraint."

Cole looked away. "I kept trying to persuade the other jurors that the
evidence was too shaky, that there was no way to know for sure that
the face in the photos was the face from the security video, but they
wouldn't listen. One of them, Jiǎng, even said, 'Who cares? He's gotta
be guilty of something. Nobody will care about another dead—'"

His breath hitched, and he winced as if in pain. "Jiǎng was one of the
jurors who restrained me as Rizzo beat the shit out of me. When he was
done, Rizzo told me that it was either Vorbes or me, and that he
didn't care if he killed a murderer or a—"

Dr. Porter nodded. "So, a homophobe and a racist. As you said, a real
piece of work."

Cole smiled at that, as if grateful for Porter's
understanding. "Yeah."

Dr. Porter gently patted one of Cole's hands. "I think you know why
you're here, and why you're having recurring nightmares."

Cole looked away again. "I'm a murderer. Sure, I was afraid for my
life, but I threw somebody who might not have been guilty of anything
under a maglev to save my own ass. If I had been man enough—"

"Stop that," Dr. Porter snapped out the command. "You're regressing,
Mr. Cole, back into the toxic masculinity that brought you to my
office after your partner Patrick's suicide. The problem isn't with
your manhood."

"No, it isn't," said Cole. "I could have saved Jean Vorbes. All I had
to do was hang the jury, cause a mistrial, and go public with my
doubts. But I was weak."

Sobs racked Cole as Dr. Porter stood impassive, waiting for the storm
to pass. It seemed obvious to Porter that Cole should have found a way
to contact an Adversary, one of the Phoenix Society's civil liberties
defense officers, but it was too late for Jean Vorbes—and too late for
Mr. Cole as well. "Mr. Cole, I think you know what you must do next."

"You expect me to contact the Adversaries," said Cole, whispering as
if speaking too loudly would summon them. "Why can't you do it?"

Dr. Porter shook his head. "I won't turn you in. Your moral cowardice
cost one person their life. I will not let it compromise my
integrity. I don't rat out my patients." He placed a hand on Cole's
shoulder before continuing, "When we first started working together, I
told you I might occasionally come down on you with harsh truths."

"Yeah."

"This is one of those occasions. The only way you can redress this
injustice is by turning yourself in, and testifying against the judge,
the DA, the cops involved, and your fellow jurors. Chances are it's
gonna cost you your life. If your fellow jurors don't lynch you, you
might be executed for murder. If you're really lucky, you might just
be exiled from Gaea instead, and consigned to a life carving habitats
out of one of the rocks between Ares and Zeus."

"But we can still talk, right? It's not like I won't have money."

Dr. Porter shook his head. "No. This is our last session. Your problem
isn't something I can fix with medicine or talk therapy."

This last shocked Cole, who shot to his feet and clenched his
fists. "You can't just abandon me like Patrick--"

"Nobody abandoned you. When your partner came out as trans, you
rejected him, and he died of heartbreak. When you had a chance to save
somebody's life, you choked and let yourself be put in a position
where you had to consent to another person's murder at the hands of
the state to save your own life. You were never here for therapy. You
were here for absolution, and that's not a service I provide. Try a
priest, instead."

"You can't speak to me like that!"

"I just did. Now get out of my office."

Once he was alone, Dr. Porter sat at his desk and spent several
minutes gazing at the Manhattan skyline through pouring rain. Once the
rain had ceased its staccato spatting against the plate glass windows,
he turned back toward his desk and picked up the phone. "Ms. Montez,
do I have any other appointments for today?"

"Just a four-thirty with Mrs. Watanabe."

"Cancel it. Take the rest of the day off, but before you leave make a
note that Jude Cole is no longer welcome here."

"Noted. Are you sure about me leaving early, Dr. Porter?"

"Yeah," Dr. Porter pulled a bottle of whiskey from the bottom left
drawer of his desk, which also held an 11.43mm revolver in a belted
holster and a box of ammunition. He poured two fingers into an Ohrmazd
Medical Group promotional mug and swirled it around before taking a
sip. "That last session left a vile taste in my mouth."

"All right," said Ms. Montez. "I know you prefer to keep things
strictly professional, but I hope you feel better tomorrow. Should I
check on you tonight?"

Unsure of whether it was the liquor, Dr. Porter smiled at his
assistant's concern. "Only if you feel you must. Just be careful out
there; it's been raining."

He hung up, and swiveled his chair to face the window again. 

## II

He was still sitting that way two hours later when somebody knocked on
the door. Turning to face it, he said, "Come in."

First to enter was a well-built man of medium height with
close-cropped brown hair, and an equally well-groomed beard and
mustache. A woman half a head taller than him followed, her expression
that of a woman used to command. Both wore uniforms that reminded
Dr. Porter of naval officers, and had swords on their hips. They were
two of the Phoenix Society's sworn blades, the Adversaries.

The woman spoke first, her accent showing only a hint of her Quebeçois
roots. "Hello again, [Dr. Porter](/characters/porter-gilbert.html). I
trust you remember us?"

"[Iris Deschat](/characters/deschat-iris.html) and [Saul
Rosenbaum](/characters/rosenbaum-saul.html), right?" Dr. Porter
decided he needed another drink. Retrieving his whiskey from the
drawer, he scrounged around for wax paper cups and found two. "Either
of you care for a drink?"

"This is an official visit," said Iris, giving Saul a pointed look as
if she expected him to say yes.

"We won't be here long, in any case," said Saul.

Only a little perturbed by the Adversaries' rejection of his
hospitality, Dr. Porter poured a finger for himself. It was less than
what he wanted, but he suspected restraint might be wise in this
situation. "How official a visit is this? Am I under arrest?"

"Of course not," said Iris. "This is about your most recent patient,
[Jude Cole](/characters/cole-jude.html). I trust you'll be pleased to
know he took your advice and came to us."

"That doesn't explain why you're here in my office."

"We're not especially interested in Jude Cole," said Saul. "Yes, he's
guilty of participating in the judicial murder of Jean Vorbes, but
putting him, the other jurors, and Judge Caldera on a one-way express
to the ass-end of the solar system isn't enough to redress the
injustice Mr. Vorbes has suffered."

"We're after Brooklyn district attorney [Jason
Lane](/characters/lane-jason.html)," said Iris, "and Mr. Cole can
testify to the methods Lane uses to railroad his enemies."

"Wait a damn minute," said Dr. Porter, wishing he dared take another
drink, this time straight from the bottle. "Are you telling me the
local DA is using the goddamn courts to commit legal murder by proxy?"

The Adversaries nodded. "That's exactly what we're telling you. Jean
Vorbes tried to blow the whistle, and ended up a victim. With Mr. Cole
willing to testify against Jason Lane, we can finally present a
compelling narrative that will persuade a jury to convict him," said
Iris.

"There's just one problem," said Saul. "It won't be hard for Lane's
attorneys to find out that Cole has been in and out of therapy his
whole life, and they'll attack his credibility as a witness."

"So we want you to testify as an expert witness on Cole's behalf, to
preclude attacks from the mental health angle," Iris added. "With his
own therapist testifying that he isn't the wrong kind of crazy,
Mr. Cole will be the perfect witness: a young gay man who—"

"—stood by and let his partner spiral into depression and commit
suicide because he couldn't accept that his partner wasn't a man, but
a trans woman?" The words came in a bitter flood, surprising
Dr. Porter. It was as if his patient's behavior had become a personal
affront.

Saul remained unfazed. "I think Iris was about to say that Mr. Cole is
a young man who has made some terrible mistakes, realizes it, and is
determined to make what restitution he can."

"He wants absolution."

"Who wouldn't," said Iris. She favored Dr. Porter with a meaningful
look. "I'm sure you wrestle with your own conscience at least once in
a while, Dr. Porter, because of some of the things you had to do to
survive Nationfall."

Dr. Porter poured himself another drink, filling his glass halfway in
defiance of present company. He raised the glass in salute to Iris
before drinking half at a gulp. "Were you a psychologist before you
become an Adversary?"

Iris shook her head. "We both served in the Commonwealth Navy before…"

Dr. Porter knew what that trailing before meant. Before
[Nationfall](/history/nationfall.html). Before everything fell
apart. Before friends, family, and neighbors who had gotten [the
Patch](/technology/the-patch.html) despite all warnings from those
skeptical of the authorities' promises descended into murderous,
cannibalistic psychosis. Before he had found himself forced to choose
between gunning down his own family or letting them feed upon him.

He finished his drink. "What did you have to do to survive?"

Iris shrugged. "We had it relatively easy. We just had to cope with
the fact that the nation we had sworn to defend no longer
existed. There was no going home now; the ship and her crew were all
we had."

"Absolution isn't an option for any of us," said Saul. "But when I
joined the Navy, my old rabbi told me that I had a greater
responsibility than the oath of enlistment I had taken on. She said
that the world was broken, and it was up to each of us to heal
it. Mr. Cole is trying to help heal the world, Dr. Porter. Are you
going to help, or are you just going to let the world stay broken?"

Dr. Porter considered the nearly empty bottle. "You'll know my answer
come the day."

Saul made to speak, but Dr. Porter held up a hand. "Don't say
anything, Adversary. Right now, you can honestly testify that you came
to request my help, and I was noncommittal. Right now, you have
plausible deniability. Don't throw it away."

Iris put what seemed to Dr. Porter a surprisingly affectionate hand on
Saul's shoulder. "He's right, Saul."

## III

A month later, [Dr. Porter](/characters/porter-gilbert.html) attended
the trial of former New York prosecutor Jason Lane for tyranny and a
litany of lesser charges. To his surprise, his former patient [Jude
Cole](/characters/cole-jude.html) had proved a compelling witness who
stalwartly resisted the defense attorneys' best efforts to discredit
him during cross examination.

"The defense calls Dr. Gilbert Porter to the stand."

Dr. Porter met the Adversaries' eyes as he approached the stand, first
Saul's and then Iris', but there was no time to reassure them as to
his intentions. They would know soon enough. Once he was seated, the
bailiff stood in front him. "Raise your right hand, please."

As Dr. Porter did so, the bailiff continued. "Dr. Gilbert Porter, do
you solemnly affirm that you will tell the truth, the whole truth, and
nothing but the truth as you understand it?"

"I do so affirm," said Dr. Porter, his gaze fixed on Jason Lane's
attorney, a tall, sallow man with fishy eyes whose law firm Porter had
taken to calling Squamous & Rugose, Attorneys at Law.

The defense attorney spoke in a thin, reedy voice. "Dr. Porter, was
Jude Cole a patient of yours?"

"He was."

"What, precisely, was the reason he came to you for help?"

"Guilt."

"And is guilt a recognized psychiatric disorder?"

"Only when the patient feels that their guilt is excessive and
negatively impacts their quality of life."

"Did you prescribe any medications for Mr. Cole's guilt, Dr. Porter?"

Dr. Porter leaned foward. "I would like the record to reflect that
while, as a psychiatrist I am not only a psychotherapist but a medical
doctor and thus qualified to make medical diagnoses and prescribe
drugs, I prefer not to do so unless it is in the patient's best
interest to do so. Therefore, I did not prescribe any medications for
Mr. Cole."

"What, then was the nature of your treatment of Mr. Cole?"

"Talk therapy and cognitive-behavioral therapy," said Dr. Porter.

The defense attorney shuffled his notes. "In your professional
opinion, Dr. Porter, is it reasonable to assume that Mr. Cole might
have a personality disorder."

"Objection, your honor," said Iris, as she rose from her
seat. "Defense is attempting to lead the witness."

"Overruled." The judge fixed a hard glare on the defense
attorney. "I'll allow the question in the interest of getting
Dr. Porter's assessment on record."

"Thank you, your honor," said Lane's attorney. "Dr. Porter, is it—"

"I heard you the first time," said Dr. Porter, "Mr. Cole has
tendencies toward selfishness, narcissism, and cowardice. None of
these qualities make him a liar. Moreover, he has overcome each of
them in order to testify over the last three days. He's a young man
who has made terrible mistakes, and has stepped up to take
responsibility for them. In my professional opinion, his credibility
is impeccable."

Out of the corner of his eye, Dr. Porter caught Jason Lane staring
daggers at his own attorney. He favored the disgraced district
attorney with a slow, lazy smile. Because the media had all but
lynched the bastard, Lane's career was over even if the jury acquitted
him.

"The defense has no further questions for Dr. Porter."

"Would the prosecution like to cross-examine?"

Saul rose, and gave Dr. Porter an approving nod. "I don't think that
will be necessary, your honor. We had hoped Dr. Porter would provide
testimony confirming the credibility of Mr. Cole's testimony on our
behalf, and he has done so."

## IV

To his surprise, [Dr. Porter](/characters/porter-gilbert.html) found
himself sitting beside [Jude Cole](/characters/cole-jude.html) three
days later, when the jury gave its verdict. Representing them was
kindly looking grandmother with steel in her spine. She stood up
straight and looked directly into Jason Lane's eyes as the judge asked
her if the jury had delivered a verdict.

"Yes, we have," said the jury's representative. "Your honor, the
charges against former Brooklyn district attorney Jason Lane are seven
counts each of tyranny, murder in the first degree, conspiracy to
commit murder, fabrication of evidence, conspiracy to fabricate
evidence, conspiracy to commit jury tampering, and subversion of
justice. We find Jason Lane guilty as charged, and recommend a life
sentence in exile."

"Thank you, Mrs. Ritter. You and your fellow jurors are to be
commended for your service," said Judge Tormundsdottir. She turned her
attention to the defendant. "Jason Lane, do you understand that you
have been found guilty on every charge against you?"

Lane stood defiant. "I understand, your honor, that I'm being
persecuted on the testimony of an overgrown boy who wasn't man enough
to see justice done with his own hands."

Judge Tormundsdottir smiled at that. "Tell yourself what you must to
ease your conscience, Mr. Lane. You're going to have to live with it
for a long time to come."

Lane shrugged. "So, you don't have the balls to see justice done,
either?"

"I just had this robe dry-cleaned, and I won't have it bloodied for
your sake," said Judge Tormundsdottir. "Jason Lane, you are hereby
sentenced to exile in perpetuity from the planet Gaea. You will be
transported to an artificial habitat orbiting the planet Zeus to live
out the rest of your natural life. Once you have arrived, you will not
be permitted to leave your new home unless this conviction is struck
down on appeal."

"You—"

Judge Tormundsdottir cut him off with a sharp swing of her
gavel. "This court is adjourned."

As the bailiff led Lane out in handcuffs, Dr. Porter studied Jude
Cole. "So, it's finally over."

Cole flashed a small smile. "Hardly. The Phoenix Society decided to
nail Jason Lane first, but there are also the cops involved, the
forensics people who helped trump up the evidence, and the judges and
jurors who carried out the executions. I'm going to be giving
testimony at a great many trials over the next few years."

"That's going to be hard for you, having to relive your own guilt on
the stand over and over again."

"Yeah," said Cole. "And when it's all over the Phoenix Society is
going to arrange transport to a habitat orbiting Aphrodite for a new
start. It would help if I had somebody to talk to in the meantime."

Dr. Porter took a business card from his coat pocket and handed it
over. "Make an appointment."

![a photograph of a man sitting on a couch](/images/nik-shuliahin-251237-unsplash.jpg)
"Hard Times": Photo by [Nik Shuliahin](https://unsplash.com/photos/BuNWp1bL0nc) on [Unsplash](https://unsplash.com)

If you enjoyed reading "Thirteen Cuts", please consider [a $1
donation](https://cash.me/$starbreaker/1). Thank you.
